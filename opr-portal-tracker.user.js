// ==UserScript==
// @name            OPR Portal Tracker
// @version         0.6.0
// @description     Tracking of OPR portal candidates
// @homepageURL     https://gitlab.com/moriakaice/opr-portal-tracker
// @author          Mori
// @match           https://opr.ingress.com/*
// @grant           unsafeWindow
// @grant           GM_addStyle
// @grant           GM_getResourceText
// @grant           GM_setClipboard
// @downloadURL     https://gitlab.com/moriakaice/opr-portal-tracker/raw/master/opr-portal-tracker.user.js
// @updateURL       https://gitlab.com/moriakaice/opr-portal-tracker/raw/master/opr-portal-tracker.user.js
// @supportURL      https://gitlab.com/moriakaice/opr-portal-tracker/issues
// @require         https://unpkg.com/leaflet@1.3.4/dist/leaflet.js
// @require         https://unpkg.com/leaflet.markercluster@1.4.0/dist/leaflet.markercluster.js
// @require         https://raw.githubusercontent.com/pieroxy/lz-string/master/libs/lz-string.min.js
// @resource        leafletCSS https://unpkg.com/leaflet@1.3.4/dist/leaflet.css
// @resource        marketClusterCSS https://unpkg.com/leaflet.markercluster@1.4.0/dist/MarkerCluster.css
// @resource        marketClusterDefaultCSS https://unpkg.com/leaflet.markercluster@1.4.0/dist/MarkerCluster.Default.css

// ==/UserScript==

/*
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

(function () {
  const w = typeof unsafeWindow === 'undefined' ? window : unsafeWindow

  if (!w.document.querySelector('#player_stats')) {
    console.error('[OPR PT] Agent not logged in, exiting')
    return
  }

  const playerName = document.querySelector('.player_nickname').innerText

  GM_addStyle(GM_getResourceText('leafletCSS'))
  GM_addStyle(GM_getResourceText('marketClusterCSS'))
  GM_addStyle(GM_getResourceText('marketClusterDefaultCSS'))

  const OPRPT = {
    SEEN_PORTALS: 'oprpt_seen_portals',
    SEEN_EDITS: 'oprpt_seen_edits',
    CONFIG: 'oprpt_config',
  }

  function initializeDbs() {
    Object.keys(OPRPT).forEach(key => {
      const defaultValue = w.localStorage.getItem(OPRPT[key]) || '{}'

      if (!w.localStorage.getItem(`${OPRPT[key]}-${playerName}`)) {
        w.localStorage.setItem(`${OPRPT[key]}-${playerName}`, defaultValue)
        w.localStorage.removeItem(OPRPT[key])
      }
    })
  }

  function getDb(key) {
    const decompress = value => value[0] === '{' ? value : LZString.decompressFromUTF16(value)

    return JSON.parse(decompress(w.localStorage.getItem(`${key}-${playerName}`) || '{}'))
  }

  function saveDbs() {
    w.localStorage.setItem(`${OPRPT.SEEN_PORTALS}-${playerName}`, LZString.compressToUTF16(JSON.stringify(db.seenPortals)))
    w.localStorage.setItem(`${OPRPT.SEEN_EDITS}-${playerName}`, LZString.compressToUTF16(JSON.stringify(db.seenEdits)))
  }

  function saveConfig() {
    w.localStorage.setItem(`${OPRPT.CONFIG}-${playerName}`, JSON.stringify(config))
  }

  initializeDbs()

  const db = {
    seenPortals: getDb(OPRPT.SEEN_PORTALS),
    seenEdits: getDb(OPRPT.SEEN_EDITS),
  }

  const config = getDb(OPRPT.CONFIG)

  function escapeText(text) {
    return text.replace(/\&/g, '&amp;').replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace(/\"/g, '&quot;').replace(/\'/g, '&apos;')
  }

  function getDate(timestamp) {
    const leadingZeros = value => ('00' + value).slice(-2)
    const time = new Date(timestamp * 1000)

    return `${leadingZeros(time.getDate())}.${leadingZeros(time.getMonth() + 1)}.${time.getFullYear()} ${leadingZeros(time.getHours())}:${leadingZeros(time.getMinutes())}`
  }

  function getTrackedPortals(reviewType, type) {
    const portals = reviewType === 'edit' ? db.seenEdits : db.seenPortals
    type = type ? type : 'plain'

    let ret

    if (type === 'geojson') {
      ret = {
        'type': 'FeatureCollection',
        'features': []
      }

      ret.features = Object.keys(portals).map(key => (
        {
          'type': 'Feature',
          'properties': {
            'name': portals[key].title,
            'description': portals[key].description,
            'imageUrl': portals[key].imageUrl,
            'time': getDate(portals[key].time),
          },
          'geometry': {
            'type': 'Point',
            'coordinates': [
              portals[key].lng,
              portals[key].lat
            ]
          }
        }
      ))
    } else if (type === 'kml') {
      const placemarks = Object.keys(portals).map(key => `
    <Placemark>
      <name>${escapeText(portals[key].title)}</name>
      <description>
      <![CDATA[
        <a href="${portals[key].imageUrl.replace('http://', 'https://')}=s0" target="_blank">
          <img class="portalDescriptionImage" src="${portals[key].imageUrl.replace('http://', 'https://')}" alt="Portal image" />
        </a><br />
        ${escapeText(portals[key].description)}<br />
        Seen in OPR: ${getDate(portals[key].time)}
      ]]>
      </description>
      <TimeStamp>
        <when>${new Date(portals[key].time * 1000).toISOString()}</when>
      </TimeStamp>
      <Point>
        <coordinates>${portals[key].lng},${portals[key].lat},0</coordinates>
      </Point>
    </Placemark>`)

      ret = `<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <name>OPR Portal Tracker</name>
    <open>1</open>${placemarks.join('')}
  </Document>
</kml>
      `
    }

    return ret ? ret : portals
  }

  w.getTrackedPortals = getTrackedPortals

  function initControls() {
    function copyGeoJson(reviewType) {
      GM_setClipboard(JSON.stringify(getTrackedPortals(reviewType, 'geojson')))
    }

    GM_addStyle(`
      .oprpt_controls {
        position: absolute;
        top: 45px;
        margin-left: -400px;
      }
      .margin-zero {
        margin: 0px !important;
      }
      .fakeButton {
        display: inline-block !important;
        margin: 0px !important;
        text-shadow: none !important;
      }
    `)

    w.document.querySelector('#player_stats:not(.visible-xs)').insertAdjacentHTML('beforeEnd', `<div class="oprpt_controls">
      <p>
        OPR Portal Tracker stats: Portals: ${Object.keys(db.seenPortals).length} &middot; Edits: ${Object.keys(db.seenEdits).length}
      </p>
      <p>
        <button class="button btn btn-default margin-zero" id="openOPRPTMap">Open map</button>
        <button class="button btn btn-default margin-zero" id="oprptClustering">Clustering: ${config.clustering ? 'ON' : 'OFF'}</button>
      </p>
      <p>
        Copy (GeoJSON):
          <button class="button btn btn-default margin-zero" id="copyTrackedPortals">Portals</button>
          <button class="button btn btn-default margin-zero" id="copyTrackedEdits">Edits</button>
        <br />
      </p>
      <p>
        Download (GeoJSON):
          <a class="button btn btn-default fakeButton" href="data:application/octet-stream;charset=utf-8,${encodeURIComponent(JSON.stringify(getTrackedPortals('new', 'geojson')))}" role="button" download="portals.geojson">Portals</a>
          <a class="button btn btn-default fakeButton" href="data:application/octet-stream;charset=utf-8,${encodeURIComponent(JSON.stringify(getTrackedPortals('edit', 'geojson')))}" role="button" download="edits.geojson">Edits</a>
        <br />
      </p>
      <p>
        Download (KML):
          <a class="button btn btn-default fakeButton" href="data:application/octet-stream;charset=utf-8,${encodeURIComponent(getTrackedPortals('new', 'kml'))}" role="button" download="portals.kml">Portals</a>
          <a class="button btn btn-default fakeButton" href="data:application/octet-stream;charset=utf-8,${encodeURIComponent(getTrackedPortals('edit', 'kml'))}" role="button" download="edits.kml">Edits</a>
        <br />
      </p>
    </div>
    `)

    w.document.getElementById('copyTrackedPortals').addEventListener('click', ev => {
      copyGeoJson('new')
    })
    w.document.getElementById('copyTrackedEdits').addEventListener('click', ev => {
      copyGeoJson('edit')
    })
    w.document.getElementById('openOPRPTMap').addEventListener('click', ev => {
      w.location.hash = 'oprpt'
      w.location.reload()
    })
    w.document.getElementById('oprptClustering').addEventListener('click', ev => {
      config.clustering = !config.clustering
      saveConfig()
      w.document.getElementById('oprptClustering').innerText = `Clustering: ${config.clustering ? 'ON' : 'OFF'}`
    })
  }

  function init() {
    let tryNumber = 15

    const initWatcher = setInterval(() => {
      if (tryNumber === 0) {
        clearInterval(initWatcher)
        w.document.getElementById('NewSubmissionController')
          .insertAdjacentHTML('afterBegin', `
    <div class='alert alert-danger'><strong><span class='glyphicon glyphicon-remove'></span> OPR Portal Tracker initialization failed, refresh page</strong></div>
    `)
        return
      }

      if (w.angular) {
        let err = false
        try {
          initAngular()
        }
        catch (error) {
          console.log(error)
          err = error
        }

        if (!err) {
          try {
            initScript()
            clearInterval(initWatcher)
          } catch (error) {
            console.log(error)

            if (error !== 42) {
              clearInterval(initWatcher)
            }
          }
        }
      }
      tryNumber--
    }, 1000)

    function initAngular() {
      const el = w.document.querySelector('[ng-app="portalApp"]')
      w.$app = w.angular.element(el)
      w.$injector = w.$app.injector()
      w.$rootScope = w.$app.scope()

      w.$scope = element => w.angular.element(element).scope()
    }

    function initScript() {
      const subMissionDiv = w.document.getElementById('NewSubmissionController')
      const subController = w.$scope(subMissionDiv).subCtrl
      const newPortalData = subController.pageData

      if (subController.errorMessage !== '') {
        // no portal analysis data available
        throw 41 // @todo better error code
      }

      if (typeof newPortalData === 'undefined') {
        // no submission data present
        throw 42 // @todo better error code
      }

      trackPortal(subController.reviewType, newPortalData)
    }

    async function createPortalHash(newPortalData) {
      // Hex function for ArrayBuffer
      const hex = buff => {
        return [].map.call(new Uint8Array(buff), b => ('00' + b.toString(16)).slice(-2)).join('')
      }

      const portal = {
        description: newPortalData.description,
        imageUrl: newPortalData.imageUrl,
        lat: newPortalData.lat,
        lng: newPortalData.lng,
        title: newPortalData.title,
        titleEdits: newPortalData.titleEdits,
        descriptionEdits: newPortalData.descriptionEdits,
        locationEdits: newPortalData.locationEdits,
      }

      Object.keys(portal).forEach(key => typeof portal[key] === 'undefined' && delete portal[key])

      return hex(await w.crypto.subtle.digest('SHA-256', new TextEncoder().encode(JSON.stringify(portal))))
    }

    async function trackPortal(reviewType, newPortalData) {
      const hash = await createPortalHash(newPortalData)

      const dbEntry = {
        description: newPortalData.description,
        imageUrl: newPortalData.imageUrl,
        lat: newPortalData.lat,
        lng: newPortalData.lng,
        title: newPortalData.title,
        titleEdits: newPortalData.titleEdits,
        descriptionEdits: newPortalData.descriptionEdits,
        locationEdits: newPortalData.locationEdits,
        time: Math.round(new Date().getTime() / 1000),
      }

      Object.keys(dbEntry).forEach(key => typeof dbEntry[key] === 'undefined' && delete dbEntry[key])

      if (reviewType === 'NEW' && !db.seenPortals[hash]) {
        db.seenPortals[hash] = dbEntry
      } else if (reviewType === 'EDIT' && !db.seenEdits[hash]) {
        db.seenEdits[hash] = dbEntry
      }

      saveDbs()
    }
  }

  function initMap() {
    w.document.body.innerHTML = `
      <div class="container">
        <div id="mapid" style="height: 100vh;"></div>
        <div class="closeMap">
          Close map
        </div>
      </div>
    `

    GM_addStyle(`
      .leaflet-retina .leaflet-control-layers-toggle {
        background-image: url(https://unpkg.com/leaflet@1.3.4/dist/images/layers-2x.png)
      }

      .leaflet-control-layers-toggle {
        background-image: url(https://unpkg.com/leaflet@1.3.4/dist/images/layers.png)
      }

      .container {
        position: relative;
      }

      .portalDescriptionImage {
        width: 250px;
      }

      .closeMap {
        position: absolute;
        left: 10px;
        bottom: 10px;
        background-color: white;
        padding: 5px;
        z-index: 999;
        color: black;
        cursor: pointer;
      }

      .marker-cluster div {
        color: #333;
      }
    `)

    L.Icon.Default.imagePath = 'https://unpkg.com/leaflet@1.3.4/dist/images/'

    const map = L.map('mapid')

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
    }).addTo(map)

    map.locate({ setView: true, maxZoom: 6 })

    const layers = {
      'Portals': null,
      'Edits': null,
    }

    function addToLayer(layerKey, portalList) {
      if (Object.keys(portalList).length) {
        const markers = []

        Object.keys(portalList).forEach(key => {
          const marker = L.marker(
            [portalList[key].lat, portalList[key].lng],
            { title: portalList[key].title }
          )

          marker.bindPopup(`
            <div>
              <h2>${escapeText(portalList[key].title)}</h2>
              <a href="${portalList[key].imageUrl.replace('http://', 'https://')}=s0" target="_blank">
                <img class="portalDescriptionImage" src="${portalList[key].imageUrl.replace('http://', 'https://')}" alt="Portal image" />
              </a>
              <p>
                ${escapeText(portalList[key].description)}
              </p>
              <p>
                Seen in OPR: ${getDate(portalList[key].time)}
              </p>
              <p>
                <a href="https://www.ingress.com/intel?ll=${portalList[key].lat},${portalList[key].lng}&z=17&pll=${portalList[key].lat},${portalList[key].lng}" target="_blank">Portal link</a> &middot;
                <a href="https://maps.google.com/maps?ll=${portalList[key].lat},${portalList[key].lng}&q=${portalList[key].lat},${portalList[key].lng}%20(${encodeURIComponent(portalList[key].title)}" target="_blank">Google Maps link</a>
              </p>
            </div>
          `, { maxWidth: 'auto' }).openPopup()

          markers.push(marker)
        })

        if (config.clustering) {
          layers[layerKey] = L.markerClusterGroup().addLayers(markers)
        } else {
          layers[layerKey] = L.layerGroup(markers)
        }
      } else {
        layers[layerKey] = L.layerGroup([])
      }
    }

    addToLayer('Portals', db.seenPortals)
    addToLayer('Edits', db.seenEdits)

    layers.Portals.addTo(map)
    L.control.layers(layers).addTo(map)

    w.document.querySelector('.closeMap').addEventListener('click', () => {
      w.location.hash = ''
      w.location.reload()
    })
  }

  if (w.location.hash.includes('oprpt')) {
    initMap()
  } else {
    initControls()

    if (w.location.pathname.includes('/recon')) {
      init()
    }
  }
})()
